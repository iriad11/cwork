## intent:greet
- hey
- hello
- hi
- good morning
- good evening
- hey there

## intent:goodbye
- bye
- goodbye
- see you around
- see you later

## intent:affirm
- yes
- indeed
- of course
- that sounds good
- correct

## intent:deny
- no
- never
- I don't think so
- don't like that
- no way
- not really

## intent:happy
- perfect
- very good
- great
- amazing
- wonderful
- I am feeling very good
- I am great
- I'm good
- Fine
- I'm fine
- I'm okay
- I am doing good
- I am doing well

## intent:ask_inventory
- what is in the inventory?
- is there anything in the inventory?
- What do you have?
- what do you have currently?
- what's in your inventory
- what's there?
- what is in there
- what is in the store
- show the products in the inventory
- tell me about the products

## intent:bot_challenge
- are you a bot?
- are you a human?
- am I talking to a bot?
- am I talking to a human?
