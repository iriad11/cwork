## path 1
* greet
  - utter_greet
* happy
  - utter_happy

## path 2
* greet
  - utter_greet
* happy
  - utter_happy
  - utter_help
* deny
  - utter_goodbye

## path 3
* greet
  - utter_greet
* happy
  - utter_happy
  - utter_help
* affirm
  - utter_inventory
  - action_inventory

## path 4
* goodbye
  - utter_goodbye

## path 5
* bot_challenge
  - utter_iamabot

## path 6
* ask_inventory
  - utter_inventory
  - action_inventory