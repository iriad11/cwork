# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/core/actions/#custom-actions/


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
import requests


class action_inventory(Action):

    def name(self) -> Text:
        return "action_inventory"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        dispatcher.utter_message("Product List:")

        response = requests.get('https://inventory-python.herokuapp.com/api/items/')
        data = response.json()
        for d in data:
            dispatcher.utter_message("Product name : " + d['product_name'] + '\n' +
                "Product price : " + str(d['product_price']) + '\n' +
                    "Stock : " + str(d['number_in_stock']) + '\n')

        return []
