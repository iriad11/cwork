# Asked Screenshots

## Inventory project

![Inventory: product list](https://bitbucket.org/iriad11/cwork/raw/96b7b120bdd337ba0e76991e4375e55ae1628090/images/inventory_1.png)

![Inventory: product detail](https://bitbucket.org/iriad11/cwork/raw/96b7b120bdd337ba0e76991e4375e55ae1628090/images/inventory_2.png)

## Rest API from Django

![Rest API](https://bitbucket.org/iriad11/cwork/raw/96b7b120bdd337ba0e76991e4375e55ae1628090/images/rest_django.png)

## RASA chat app with custom action

![Rasa Chat](https://bitbucket.org/iriad11/cwork/raw/96b7b120bdd337ba0e76991e4375e55ae1628090/images/rasa_1.png)

![Rasa Chat custom action](https://bitbucket.org/iriad11/cwork/raw/96b7b120bdd337ba0e76991e4375e55ae1628090/images/rasa_2.png)

# Modify the "rasa-master" to connect Django

## Domain

* All intents, entity, response templates, action names are defined here.
* Edit this file to add or edit above behaviour of the chatbot.
* Need to be very careful editing this file.

## Credentials

* To deploy the chatbot credentials are needed to be put here.
* To use with Rest API no credentials is needed.

## Actions
* Custom actions are defined here in python code (Custom action to connect with Django inventory is defined here).
* Every custom action names needs to be in domain file too.

## Endpoints
* Edit `endpoints.yml` file to enable ar add various endpoints 
* Uncomment `action_endpoint` and it's url to enable custom actions defined in `actions.py` (This was performed to connect with Django inventory).
* Similarly other endpoints can be defined

## /data/nlu.md

* This file contains intents and their sample messages.
* New intents needs to be both in `domain.yml` and `nlu.md`.

## /data/stories.md

* This file contains sample conversation path.
* Action selection depends on the contents of this file

# Docker and how it works in Heroku

 Docker is a tool that allows developer to easily create, deploy and run applications using containers. It allows to package up an application with all the parts it needs such as libraries and other dependencies. It allows to ship all these out as one single package.

 Heroku comes with container registry which allow to deploy docker images to Heroku. Heroku call the container dynos and describes it as “a lightweight container running a single super-specified command”. Using Dynos, applications can be scaled to any level based on resource requirements.

# Micro-service architecture

 Micro-service architecture is one of the best achievement of software industry. It allows us maximize deployment velocity and application reliability. Micro-service architecture allows to research and invent as it allows to change one part without effecting others. Each micro-services can be used across projects or applications to reduce cost and increase effeciency. 

## Link of Heroku hosted Django Inventory

`
https://inventory-python.herokuapp.com/inventory/
`