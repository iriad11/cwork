from django.urls import path
from . import views

app_name = 'inventory'
urlpatterns = [
    # ex: /inventory/
    path('', views.index, name='index'),
    # ex: /inventory/5/
    path('<int:id>/', views.detail, name='detail'),

]