from django.shortcuts import render
from django.http import Http404
from django.http import HttpResponse
from .models import Product, ItemSerializer

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

# Create your views here.


def index(request):
    product_list = Product.objects.order_by('product_name')
    context = {'product_list': product_list}
    return render(request, 'inventory/index.html', context)

def detail(request, id):
    try:
        product = Product.objects.get(pk=id)
    except Product.DoesNotExist:
        raise Http404("Product does not exist")
    return render(request, 'inventory/detail.html', {'product': product})


@api_view(['GET'])
def product_collection(request):
    if request.method == 'GET':
        item = Product.objects.all()
        serializer = ItemSerializer(item, many=True)
        return Response(serializer.data)