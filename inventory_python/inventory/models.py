from django.db import models
from rest_framework import serializers

# Create your models here.

class Product(models.Model):
    product_name = models.CharField(max_length=100)
    number_in_stock = models.IntegerField(default=0)
    product_price = models.IntegerField(default=0)

class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('product_name', 'number_in_stock', 'product_price')